# Mobilalkalmazasok beadando

## Deadline Champions csapata

- Császár Zsolt
- Csonka László

## Az alkalmazásról

Alkalmazásunk célja, hogy a felhasználók jelszavait biztonságosan tároljuk.

## Tervezett funkciók

- Jelszavak tárolása encryptelve
- Felhasználó azonosítása különböző módokon
   - Jelszó
   - PIN
   - Biometrikus
- Online back-up, jelszavak elérése különböző eszközökről
- Adatbázis törlése távolról (pl.: elvesztett telefon esetén)